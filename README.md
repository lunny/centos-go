# Usage

This assumes that you are using go mod to build.

```shell
docker run --rm -v "$PWD":/myrepo -w /myrepo lunny/centos-go:latest go build
```

With a goproxy env

```shell
docker run --rm -v "$PWD":/myrepo -w /myrepo -e GOPROXY=goproxy.io lunny/centos-go:latest go build
```

With a shared go mod cache in host

```shell
docker run --rm -v "$PWD":/myrepo -v ~/go/pkg/mod:/modcache -w /myrepo -e GOMODCACHE=/modcache -e GOPROXY=goproxy.io lunny/centos-go:latest go build
```